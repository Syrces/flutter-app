import 'package:flutter/material.dart';

import 'package:english_words/english_words.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'dart:async';


void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Random Words',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home:RandomWords()//MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

//class MyHomePage extends StatefulWidget {
  //MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  //final String title;

  @override
  //_MyHomePageState createState() => _MyHomePageState();
//}

class RandomWordsState extends State<RandomWords> {
  //class _MyHomePageState extends State<MyHomePage> {
  int page = 1;
  //List<String> items = ['item 1', 'item 2', ];
  final  _suggestions = <WordPair> [];



  final int i=0;
  static int j=0;
  bool isLoading = false;
  @override
  void initState(){

    setState(() {
      _buildSuggestions();

    });
    super.initState();


  }
   // _buildSuggestions();





  Future _loadData() async {
    // perform fetching data delay
    await new Future.delayed(new Duration(seconds: 2));

    print("load more");
    _choose();
    // update data and loading status

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueGrey[400],
        title: Text('Random Names',
          style: GoogleFonts.lato(
              textStyle: Theme
                  .of(context)
                  .textTheme
                  .display1,
              fontSize: 28,
              fontWeight: FontWeight.w700,
              color: Colors.white),
        ),
      ),


       body: war(),

    );

  }
        Widget _not() {
      return Column(


          children: <Widget>[


        Expanded(
          child:NotificationListener<ScrollNotification>(
            onNotification: (ScrollNotification scrollInfo) {
            if (!isLoading && scrollInfo.metrics.pixels ==
              scrollInfo.metrics.maxScrollExtent) {
            _loadData();
            // start loading data
            setState(() {
              isLoading = true;
            });
          }
        },
            child: _buildSuggestions(),
      )
    ,
    ),

        Container(
          height: isLoading ? 50.0 : 0,
          color: Colors.transparent,
          child: Center(
            child: new CircularProgressIndicator(),
    ),
    ),
    ],
    );
    }








    Widget _buildSuggestions(){
      return Scrollbar(

          child: ListView.builder(
              padding: const EdgeInsets.all(16.0),
              itemCount: _suggestions.length,
              itemBuilder: (context, index) {

                return _buildRow(_suggestions[index]);


              }
              ));


    }
  Widget _buildRow(WordPair pair){
    return Card(
        child:ListTile(
          leading: Icon(FontAwesomeIcons.android),
          title: Text(


            pair.asPascalCase,
            style: GoogleFonts.lato(textStyle: Theme.of(context).textTheme.display1,
                fontSize: 22
            ),

              ),
          trailing: Icon(FontAwesomeIcons.angleRight),
          onTap: () {
            print(Icon(FontAwesomeIcons.boxOpen));
          },

        )
    );

  }
  Widget _choose(){
    setState(() {
      _suggestions.addAll(generateWordPairs().take(15));

      // print('items: '+ _suggestions.toString());//items.addAll( ['item 1']);
      //print('items: '+ items.toString());
      isLoading = false;

    });


  }
  Widget war(){
    if(j==0){
      _choose();
      j=1;
      return _buildSuggestions();
    }
    else{
      return _not();
    }
  }
}
class RandomWords extends StatefulWidget {
  @override
  RandomWordsState createState() => RandomWordsState();

}



